namespace vd.web.Helpers {
    public enum ResourceUriType {
        PreviousPage,
        NextPage,
        Current
    }
}