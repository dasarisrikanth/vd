using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using vd.web.Entittes;

namespace vd.web.Helpers
{
    public static class ResourceUriHelper
    {

        public static string CreateResourceUri (
            string routeName,
            ResourceParams param,
            ResourceUriType type,
            IUrlHelper _urlHelper) 
        {
            switch (type) {
                case ResourceUriType.PreviousPage:
                    return _urlHelper.Link (routeName, new {
                        fields = param.Fields,
                            orderBy = param.OrderBy,
                            searchQuery = param.SearchQuery,
                            Adsh = param.Adsh,
                            Statement=param.Statement,
                            Tags=param.Tags,
                            pageNumber = param.PageNumber - 1,
                            pageSize = param.PageSize
                    }).ToLowerInvariant();
                case ResourceUriType.NextPage:
                    return _urlHelper.Link (routeName, new {
                        fields = param.Fields,
                            orderBy = param.OrderBy,
                            searchQuery = param.SearchQuery,
                            Adsh = param.Adsh,
                            Statement=param.Statement,
                            Tags=param.Tags,
                            pageNumber = param.PageNumber + 1,
                            pageSize = param.PageSize
                    }).ToLowerInvariant();
                case ResourceUriType.Current:
                default:
                    return _urlHelper.Link (routeName, new {
                        fields = param.Fields,
                            orderBy = param.OrderBy,
                            searchQuery = param.SearchQuery,
                            Adsh = param.Adsh,
                            Statement=param.Statement,
                            Tags=param.Tags,
                            pageNumber = param.PageNumber,
                            pageSize = param.PageSize
                    }).ToLowerInvariant();
            }
        }


        public static IEnumerable<LinkDto> CreateLinks (string routeName,string adsh, string tags, string fields, IUrlHelper _urlHelper) 
        {
            var links = new List<LinkDto> ();
            links.Add (new LinkDto (_urlHelper.Link (routeName,new {
                                                                adsh=adsh,
                                                                fields=fields,                                                                
                                                                tags=tags
                                                            }).ToLowerInvariant(),
                                                            routeName,
                                                            "GET"));

            return links;
        }

        public  static IEnumerable<LinkDto> CreateLinksForHateos (
            string routeName,
            ResourceParams param,
            bool hasNext, 
            bool hasPrevious,
            IUrlHelper _urlHelper) 
        {
            var links = new List<LinkDto> ();

            // self 
            links.Add (
                new LinkDto (CreateResourceUri (routeName,param,ResourceUriType.Current,_urlHelper), "self", "GET"));

            if (hasNext) {
                links.Add (
                    new LinkDto (CreateResourceUri (routeName, param,ResourceUriType.NextPage,_urlHelper),"nextPage", "GET"));
            }

            if (hasPrevious) {
                links.Add (
                    new LinkDto (CreateResourceUri (routeName,param,ResourceUriType.PreviousPage,_urlHelper),"previousPage", "GET"));
            }

            return links;
        }
    }
}