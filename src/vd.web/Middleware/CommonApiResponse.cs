using System;
using System.Net;

namespace vd.web.Middleware
{
    public class CommonApiResponse
    {
        public static CommonApiResponse Create(HttpStatusCode statusCode,
                                                object result = null,
                                                string errorMessage = null)
        {
            return new CommonApiResponse(statusCode, result, errorMessage);
        }

        public string Version => "1.0.1";

        public int StatusCode {get;set;}  //Change to Enum

        public string RequestId {get;}

        public string ErrorMessage { get; set; }

        public object Result {get;set;}

        protected CommonApiResponse(HttpStatusCode statusCode,
                                    object result=null,
                                    string errorMessage=null)
        {
            RequestId=Guid.NewGuid().ToString();
            StatusCode = (int)statusCode;
            Result = result;
            ErrorMessage=errorMessage;
        }

    }
}