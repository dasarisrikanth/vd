using System.IO;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using vd.core.extensions;
using vd.web.Constants;

namespace vd.web.Middleware {
    //https://stackoverflow.com/questions/40748900/how-can-i-wrap-web-api-responsesin-net-core-for-consistency/41378131?utm_medium=organic&utm_source=google_rich_qa&utm_campaign=google_rich_qa
    public class ResponseWrapper {
        private readonly RequestDelegate _next;

        public ResponseWrapper (RequestDelegate next) {
            _next = next;
        }

        public async Task Invoke (HttpContext context) 
        {
            
                var currentBody = context.Response.Body;

                using (var memoryStream = new MemoryStream ()) 
                {
                    context.Response.Body = memoryStream;

                    await _next (context);

                    context.Response.Body = currentBody;
                    memoryStream.Seek (0, SeekOrigin.Begin);

                    var readToEnd = new StreamReader (memoryStream).ReadToEnd ();
                    
                    var content= context.IfNotNull(x=>x).Response.IfNotNull(y=>y).ContentType;

                    if(content=="application/json" || content.IfNotNull(a=>a.Contains(ConstMediaType.VD_API_HATEOAS))) 
                    {
                        var objResult = JsonConvert.DeserializeObject (readToEnd);
                        var result = CommonApiResponse.Create ((HttpStatusCode) context.Response.StatusCode, objResult, null);
                        await context.Response.WriteAsync (JsonConvert.SerializeObject (result));
                    }
                    else
                    {
                        await context.Response.WriteAsync (readToEnd);
                    }                       
                    
                }
            
        }

    }
}