using AutoMapper;
using vd.database.Entity;
using vd.web.Entittes;

namespace vd.web.MappingProfiles {
    public class PreMappingProfile : Profile {
        public PreMappingProfile () {
            CreateMap<PreDto, pre> ().ReverseMap ();
        }
    }
}