using AutoMapper;
using vd.database.Entity;
using vd.web.Entittes;

namespace vd.web.MappingProfiles {
    public class TagMappingProfile : Profile {
        public TagMappingProfile () {
            CreateMap<TagDto, tag> ().ReverseMap ();
        }
    }
}