using AutoMapper;
using vd.database.Entity;
using vd.web.Entittes;

namespace vd.web.MappingProfiles {
    public class NumMappingProfile : Profile {
        public NumMappingProfile () {
            CreateMap<NumDto, num> ().ReverseMap ();
        }
    }
}