using AutoMapper;
using vd.solr.Models;
using vd.web.Entittes.SolrModels;

namespace vd.web.MappingProfiles
{
    public class SearchMappingProfile:Profile
    {
        public SearchMappingProfile() 
        {
            CreateMap<SolrObject,SolrResultObj>().ReverseMap();
        }
    }
}