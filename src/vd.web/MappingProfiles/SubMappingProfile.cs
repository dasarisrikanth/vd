using AutoMapper;
using vd.database.Entity;
using vd.web.Entittes;

namespace vd.web.MappingProfiles {
    public class SubMappingProfile : Profile {
        public SubMappingProfile () {
            CreateMap<SubDto, submission> ().ReverseMap ();
        }
    }
}