interface LinksModel {
    href:string;
    rel:string;
    method:string;
}

interface SearchModel{
    adsh : string;
    name : string;
    links : LinksModel[];
}

interface ResponseModel {
    Version : string;
    StatusCode : number;
    RequestId : string;
    ErrorMessage? : any;
    Result : Values
}

interface Values {
    value:SearchModel[];
}




