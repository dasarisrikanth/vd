import * as React from 'react';
import { FooterSectionFeatures } from './FooterSectionFeatures';
import { FooterSectionResources } from './FooterSectionResources';


export class FooterLinks extends React.Component<{},{}> {
    public render() {
        return <footer className="container py-5">
            <div className="row">
                <FooterSectionFeatures />
                <FooterSectionResources />
                <FooterSectionResources />
            </div>
        </footer>;

    };
}