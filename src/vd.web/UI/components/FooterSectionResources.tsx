import * as React from 'react';

export class FooterSectionResources extends React.Component<{},{}> {
    public render() {
        return <div className="col-6 col-md">
        <h5>Resources</h5>
            <ul className="list-unstyled text-small">
                <li><a className="text-muted" href="#">Resource</a></li>
                <li><a className="text-muted" href="#">Resource name</a></li>
                <li><a className="text-muted" href="#">Another resource</a></li>
                <li><a className="text-muted" href="#">Final resource</a></li>
            </ul>
        </div>;        
    }
}