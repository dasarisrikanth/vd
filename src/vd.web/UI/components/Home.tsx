import * as React from 'react'
import { RouteComponentProps } from 'react-router';
import { SearchComponent } from './SearchComponent'
export class Home extends React.Component<RouteComponentProps<{}>,{}>{
    public render() {
        return <div className='position-relative overflow-hidden p-3 p-md-5 m-md-3 text-center bg-light'>
                    <div className='col-md-5 p-lg-5 mx-auto my-5'>
                    <h1 className='display-4 font-weight-normal'>Value Dashboard</h1>
                    <p className='lead font-weight-normal'>Place for old school techniques.</p>
                        <br/>
                        <SearchComponent />
                    </div>                    
                </div>;
    }
}