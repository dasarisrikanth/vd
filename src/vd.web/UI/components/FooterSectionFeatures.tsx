import * as React from 'react';

export class FooterSectionFeatures extends React.Component<{},{}> {
    public render() {
        return <div className="col-12 col-md">
        <h5>Features</h5>
            <ul className="list-unstyled text-small">
                <li><a className="text-muted" href="#">Cool stuff</a></li>
                <li><a className="text-muted" href="#">Random feature</a></li>
                <li><a className="text-muted" href="#">Team feature</a></li>
                <li><a className="text-muted" href="#">Stuff for developers</a></li>
                <li><a className="text-muted" href="#">Another one</a></li>
                <li><a className="text-muted" href="#">Last time</a></li>
            </ul>
        </div>;
        
    }
}