///<reference path="./../Models/interfaces.d.ts" />

import * as React from 'react';
//import {Typeahead} from 'react-bootstrap-typeahead';
import {AsyncTypeahead} from 'react-bootstrap-typeahead';


interface ISearchComponentState{
    isLoading:boolean;
    options:SearchModel[];
}

interface ISearchComponentProps{
    
}

export class SearchComponent extends React.Component<ISearchComponentProps,ISearchComponentState> {

    constructor(props:ISearchComponentProps){
            super(props);
            this.state = {isLoading:false, options:[]};
    }

    public handleSearch = (query:any) => {
        this.setState({isLoading: true});
            //Temporary
            fetch('api/solr/'+query,{
                headers: {
                "Accept": "application/vd.webapi.hateoas+json"              
                },
            method:"GET"})
            .then(response => response.json() as Promise<ResponseModel>)
            .then(data => {             
                console.log("what is data "+data);   
                console.log("what is version"+data.Version);
                console.log("what is result"+data.Result);
                this.setState({ isLoading: false, options: data.Result.value });      
                console.log("after setting options "+this.state.options)    
            })
            .catch(function(error) {
                console.log('Requeest Failed :'+error);  
            });

        };
    
    
    public render() {
        return <AsyncTypeahead
            clearButton
            labelKey="name"
            isLoading={this.state.isLoading}
            placeholder="Enter the company name to search..."
            onSearch={this.handleSearch}
            options={this.state.options}
            minLength={3}
        />;
      

    }
}

