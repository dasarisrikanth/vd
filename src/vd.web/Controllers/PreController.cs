using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Linq;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using vd.core.extensions;
using vd.core.Helper;
using vd.database.Entity;
using vd.web.Constants;
using vd.web.Entittes;
using vd.web.Extensions;
using vd.web.Helpers;
using vd.web.Services;

namespace vd.web.Controllers
{
    [Route("api/[Controller]")]
    public class PreController:Controller
    {

        #region Injected Memebers
        public IDataRepository dataRepository;
        private IUrlHelper _urlHelper;
        private IPropertyMappingService _propertyMappingService;
        private ITypeHelperService _typeHelperService;

        #endregion

        #region Constructor
        public PreController (IDataRepository datarepository, 
                                IUrlHelper urlHelper, 
                                IPropertyMappingService propertyMappingService, 
                                ITypeHelperService typeHelperService) 
        {
            dataRepository = datarepository;
            _urlHelper = urlHelper;
            _propertyMappingService = propertyMappingService;
            _typeHelperService = typeHelperService;
        }
        #endregion


        
        #region Operations 

        [HttpGet("{adsh}/BS/{Tags?}", Name="GetBalanceSheet")]
        [HttpHead]
        public IActionResult GetBalanceSheet(ResourceParams resourceParams, 
                                            [FromHeader (Name = "Accept")] string mediaType)
        {
            resourceParams.Statement="BS";
            return GetPresentationData(resourceParams,mediaType,"GetBalanceSheet");
        }

        [HttpGet("{adsh}/IS/{Tags?}", Name="GetIncomeStatement")]
        [HttpHead]
        public IActionResult GetIncomeStatement(ResourceParams resourceParams, 
                                            [FromHeader (Name = "Accept")] string mediaType)
        {
            resourceParams.Statement="IS";
            return GetPresentationData(resourceParams,mediaType,"GetIncomeStatement");
        }

        [HttpGet("{adsh}/CF/{Tags?}", Name="GetCashFlow")]
        [HttpHead]
        public IActionResult GetCashFlow(ResourceParams resourceParams, 
                                            [FromHeader (Name = "Accept")] string mediaType)
        {
            resourceParams.Statement="CF";
            return GetPresentationData(resourceParams,mediaType,"GetCashFlow");
        }


        [HttpGet("{adsh}/EQ/{Tags?}", Name="GetEquity")]
        [HttpHead]
        public IActionResult GetEquity(ResourceParams resourceParams, 
                                            [FromHeader (Name = "Accept")] string mediaType)
        {
            resourceParams.Statement="EQ";
            return GetPresentationData(resourceParams,mediaType,"GetEquity");
        }


        [HttpGet("{adsh}/CI/{Tags?}", Name="GetComprehensiveIncome")]
        [HttpHead]
        public IActionResult GetComprehensiveIncome(ResourceParams resourceParams, 
                                            [FromHeader (Name = "Accept")] string mediaType)
        {
            resourceParams.Statement="CI";
            return GetPresentationData(resourceParams,mediaType,"GetComprehensiveIncome");
        }

        [HttpGet("{adsh}/UN/{Tags?}", Name="GetUnclassifiable")]
        [HttpHead]
        public IActionResult GetUnclassifiable(ResourceParams resourceParams, 
                                            [FromHeader (Name = "Accept")] string mediaType)
        {
            resourceParams.Statement="UN";
            return GetPresentationData(resourceParams,mediaType,"GetUnclassifiable");
        }

        [HttpGet("{adsh}/{Statement?}/{Tags?}", Name="GetPresentationData")]
        [HttpHead]
        public IActionResult GetPresentationData(ResourceParams resourceParams, 
                                                [FromHeader (Name = "Accept")] string mediaType, string ActionName="GetPresentationData")
        {
            var resultFromRepo=dataRepository.GetPresentationData(resourceParams);

            var presentationData=Mapper.Map<IEnumerable<PreDto>>(resultFromRepo);

            if(mediaType == ConstMediaType.VD_API_HATEOAS)
            {
                var paginationMetaData=new
                {
                    totalCount = resultFromRepo.TotalCount,
                    pageSize = resultFromRepo.PageSize,
                    currentPage = resultFromRepo.CurrentPage,
                    totalPages = resultFromRepo.TotalPages
                };
                
                Response.Headers.Add ("X-Pagination", Newtonsoft.Json.JsonConvert.SerializeObject (paginationMetaData));

                var links= ResourceUriHelper.CreateLinksForHateos(ActionName,resourceParams,resultFromRepo.HasNext,resultFromRepo.HasPrevious,_urlHelper);

                var shapedPresentatinData = presentationData.ShapeData(resourceParams.Fields).ToList();

                var shapedPresentatinDataLinks = shapedPresentatinData.ToList().Select(p =>
                {
                    var presentationDataDictionary = p as IDictionary<string,object>;
                    var presentationLinks = ResourceUriHelper.CreateLinks(ActionName,(string)presentationDataDictionary["adsh"],null,resourceParams.Fields,_urlHelper);

                    presentationDataDictionary.Add("links",presentationLinks);

                    return presentationDataDictionary;
                });

                var linkedCollectionResource = new {
                    value=shapedPresentatinDataLinks, links=links
                };

                return Ok(linkedCollectionResource);

            }
            else
            {
                var previousPageLink = resultFromRepo.HasPrevious ? ResourceUriHelper.CreateResourceUri(ActionName,resourceParams,ResourceUriType.PreviousPage,_urlHelper) : null;
                var nextPageLink = resultFromRepo.HasNext ? ResourceUriHelper.CreateResourceUri(ActionName,resourceParams,ResourceUriType.NextPage,_urlHelper) : null;

                var paginationMetaData = new 
                {
                    previousPageLink = previousPageLink,
                    nextPageLink = nextPageLink,
                    totalCount=resultFromRepo.TotalCount,
                    pageSize=resultFromRepo.PageSize,
                    currentPage=resultFromRepo.CurrentPage,
                    totalPages=resultFromRepo.TotalPages
                };

                Response.Headers.Add("X-Pagination",Newtonsoft.Json.JsonConvert.SerializeObject (paginationMetaData));

                return Ok(presentationData.ShapeData(resourceParams.Fields));
            }
        }
        #endregion

        


         
        
    }
}