using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using vd.web.Constants;
using vd.web.Helpers;

namespace vd.web.Controllers
{
    [Route("api")]
    public class RootController:Controller
    {
        private IUrlHelper _urlHelper;

        public RootController(IUrlHelper urlHelper)
        {
            _urlHelper=urlHelper;
        }

        [HttpGet(Name="GetRoot")]
        public IActionResult GetRoot([FromHeader(Name="Accept")]string mediaType)
        {
            if(mediaType==ConstMediaType.VD_API_HATEOAS)
            {
                var links =new List<LinkDto>();

                links.Add(new LinkDto(_urlHelper.Link("GetRoot",new {}),"self","GET"));

                links.Add(new LinkDto(_urlHelper.Link("GetBusiness",new {}),"GetBusiness","GET"));

                links.Add(new LinkDto(_urlHelper.Link("GetBusinessByAdsh",new {}),"GetBusinessByAdsh","GET"));

                return Ok(links);
            }

            return NoContent();
        }
    }
}