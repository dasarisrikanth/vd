using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using vd.core.extensions;
using vd.solr;
using vd.solr.Models;
using vd.web.Constants;
using vd.web.Entittes;
using vd.web.Entittes.SolrModels;
using vd.web.Extensions;
using vd.web.Helpers;
using vd.web.Services;

namespace vd.web.Controllers
{
    [Route("api/[Controller]")]
    public class SolrController: Controller
    {
        #region Injected Members
        public IVdSearch _search;
        public IUrlHelper _urlHelper;
        
        private IPropertyMappingService _propertyMappingService;
        private ITypeHelperService _typeHelperService;
        #endregion

        public SolrController(IVdSearch search,
                                IUrlHelper urlHelper, 
                                IPropertyMappingService propertyMappingService, 
                                ITypeHelperService typeHelperService)
        {
            _search=search;
            _urlHelper=urlHelper;
            _propertyMappingService = propertyMappingService;
            _typeHelperService = typeHelperService;

            
        }


        #region Operations
        [HttpGet("{SearchQuery}",Name="VdSearch")]
        [HttpHead]
        public IActionResult VdSearch(ResourceParams resourceParams, [FromHeader (Name = "Accept")] string mediaType)
        {
            
            if(resourceParams.SearchQuery.IsNull()) return null;
            
            if(mediaType!=ConstMediaType.VD_API_HATEOAS) return null;

            if(!_search.IsConnected)
                _search.Connect("http://localhost:8983/solr/vdash");

            var resultfromrepo = _search.DoSearch(new SolrQueryObj(){Query=resourceParams.SearchQuery.ToLowerInvariant()});

            var list = Mapper.Map<IEnumerable<SolrObject>>(resultfromrepo.Results);

            var shapedResponse=list.ShapeData(resourceParams.Fields).ToList();

            var shapedResponseWithLinks =  shapedResponse.ToList().Select(b=>
            {
                  var bDictionary = b as IDictionary<string,object>;
                  var blinks=ResourceUriHelper.CreateLinks("GetBusiness",(string) bDictionary["adsh"],null, resourceParams.Fields,_urlHelper);

                  blinks=blinks.Concat(ResourceUriHelper.CreateLinks("GetBalanceSheet",(string)bDictionary["adsh"],null,null,_urlHelper));
                  blinks=blinks.Concat(ResourceUriHelper.CreateLinks("GetIncomeStatement",(string)bDictionary["adsh"],null,null,_urlHelper));
                  blinks=blinks.Concat(ResourceUriHelper.CreateLinks("GetCashFlow",(string)bDictionary["adsh"],null,null,_urlHelper));
                  blinks=blinks.Concat(ResourceUriHelper.CreateLinks("GetNumbers",(string)bDictionary["adsh"],null,null,_urlHelper));
                  bDictionary.Add ("links", blinks);

                  return bDictionary;

            });


                var linkedCollectionResource = new {
                    value = shapedResponseWithLinks
            };

                return Ok (linkedCollectionResource);                
            
            
        }
        #endregion
    }
}