using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using vd.web.Constants;
using vd.web.Entittes;
using vd.web.Extensions;
using vd.web.Helpers;
using vd.web.Services;

namespace vd.web.Controllers
{
    [Route("api/[Controller]")]
    public class NumController:Controller
    {
        #region Injected Memebers
        public IDataRepository dataRepository;
        private IUrlHelper _urlHelper;
        private IPropertyMappingService _propertyMappingService;
        private ITypeHelperService _typeHelperService;

        #endregion


        #region Constructor 
        public NumController(IDataRepository datarepository, 
                                    IUrlHelper urlHelper, 
                                    IPropertyMappingService propertyMappingService, 
                                    ITypeHelperService typeHelperService)
        {
                dataRepository = datarepository;
                _urlHelper = urlHelper;
                _propertyMappingService = propertyMappingService;
                _typeHelperService = typeHelperService;
        }
        #endregion



        #region Operations
        
        [HttpGet("{adsh}/{Tags?}", Name="GetNumbers")]
        [HttpHead]
        public IActionResult GetNumbers(ResourceParams resourceParams, 
                                        [FromHeader (Name = "Accept")] string mediaType)
        {
            var resultFromRepo=dataRepository.GetNumbers(resourceParams);

            var numberData = Mapper.Map<IEnumerable<NumDto>>(resultFromRepo);

            if(mediaType==ConstMediaType.VD_API_HATEOAS)
            {
                var paginationMetaData=new
                {
                    totalCount = resultFromRepo.TotalCount,
                    pageSize = resultFromRepo.PageSize,
                    currentPage = resultFromRepo.CurrentPage,
                    totalPages = resultFromRepo.TotalPages
                };
                
                Response.Headers.Add ("X-Pagination", Newtonsoft.Json.JsonConvert.SerializeObject (paginationMetaData));

                var links= ResourceUriHelper.CreateLinksForHateos("GetNumbers",resourceParams,resultFromRepo.HasNext,resultFromRepo.HasPrevious,_urlHelper);

                var shapedNumbersData = numberData.ShapeData(resourceParams.Fields).ToList();

                var shapedNumbersDataLinks = shapedNumbersData.ToList().Select(n=>
                {
                    var numberDataDictionary = n as IDictionary<string, object>;
                    var numberLinks = ResourceUriHelper.CreateLinks("GetNumbers",(string)numberDataDictionary["adsh"],null,resourceParams.Fields,_urlHelper);

                    numberDataDictionary.Add("links",numberLinks);
                    
                    return numberDataDictionary;
                });

                var linkedCollectionResource = new {
                    value=shapedNumbersDataLinks, links=links
                };

                return Ok(linkedCollectionResource);
            }
            else
            {
                var previousPageLink = resultFromRepo.HasPrevious ? ResourceUriHelper.CreateResourceUri("GetNumbers",resourceParams,ResourceUriType.PreviousPage,_urlHelper) : null;
                var nextPageLink = resultFromRepo.HasNext ? ResourceUriHelper.CreateResourceUri("GetNumbers",resourceParams,ResourceUriType.NextPage,_urlHelper) : null;

                var paginationMetaData = new 
                {
                    previousPageLink = previousPageLink,
                    nextPageLink = nextPageLink,
                    totalCount=resultFromRepo.TotalCount,
                    pageSize=resultFromRepo.PageSize,
                    currentPage=resultFromRepo.CurrentPage,
                    totalPages=resultFromRepo.TotalPages
                };

                Response.Headers.Add("X-Pagination",Newtonsoft.Json.JsonConvert.SerializeObject (paginationMetaData));

                return Ok(numberData.ShapeData(resourceParams.Fields));
            }
        }

        #endregion

        
    }
}