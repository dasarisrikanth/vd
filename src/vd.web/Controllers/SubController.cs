using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Diagnostics.Contracts;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using vd.core.extensions;
using vd.core.Helper;
using vd.database;
using vd.database.Entity;
using vd.web.Constants;
using vd.web.Entittes;
using vd.web.Extensions;
using vd.web.Helpers;
using vd.web.Services;

namespace vd.web.Controllers 
{
    [Route ("api/[Controller]")]
    public class SubController : Controller 
    {
        #region Injected Members
        public IDataRepository dataRepository;
        private IUrlHelper _urlHelper;
        private IPropertyMappingService _propertyMappingService;
        private ITypeHelperService _typeHelperService;
        #endregion

        #region Constructor
        public SubController (IDataRepository datarepository, IUrlHelper urlHelper, 
                                IPropertyMappingService propertyMappingService, 
                                ITypeHelperService typeHelperService) 
        {
            dataRepository = datarepository;
            _urlHelper = urlHelper;
            _propertyMappingService = propertyMappingService;
            _typeHelperService = typeHelperService;
        }
        #endregion

        #region Operations
        
        [HttpGet("{adsh?}", Name="GetBusiness")]
        [HttpHead]
        public IActionResult GetBusiness (ResourceParams resourceParams, [FromHeader (Name = "Accept")] string mediaType) 
        {
            //if(resourceParams.IsNotNull())

            var resultFromRepo = dataRepository.GetBusiness (resourceParams);

            var businessList = Mapper.Map<IEnumerable<SubDto>> (resultFromRepo);

            if (mediaType == ConstMediaType.VD_API_HATEOAS) {
                var paginationMetaData = new {
                totalCount = resultFromRepo.TotalCount,
                pageSize = resultFromRepo.PageSize,
                currentPage = resultFromRepo.CurrentPage,
                totalPages = resultFromRepo.TotalPages
                };

                Response.Headers.Add ("X-Pagination", Newtonsoft.Json.JsonConvert.SerializeObject (paginationMetaData));

                var links = ResourceUriHelper.CreateLinksForHateos ("GetBusiness",resourceParams, resultFromRepo.HasNext, resultFromRepo.HasPrevious,_urlHelper);

                var shapedBusiness = businessList.ShapeData (resourceParams.Fields).ToList();

                var shapedBusinessWithLinks = shapedBusiness.ToList().Select (business => 
                {

                    var businessDictionary = business as IDictionary<string, object>;
                    var businessLinks = ResourceUriHelper.CreateLinks("GetBusiness",(string) businessDictionary["adsh"],null, resourceParams.Fields,_urlHelper);
                    businessLinks=businessLinks.Concat(ResourceUriHelper.CreateLinks("GetBalanceSheet",(string)businessDictionary["adsh"],null,null,_urlHelper));
                    businessLinks=businessLinks.Concat(ResourceUriHelper.CreateLinks("GetIncomeStatement",(string)businessDictionary["adsh"],null,null,_urlHelper));
                    businessLinks=businessLinks.Concat(ResourceUriHelper.CreateLinks("GetCashFlow",(string)businessDictionary["adsh"],null,null,_urlHelper));
                    businessLinks=businessLinks.Concat(ResourceUriHelper.CreateLinks("GetNumbers",(string)businessDictionary["adsh"],null,null,_urlHelper));
                    businessDictionary.Add ("links", businessLinks);

                    return businessDictionary;
                });

                var linkedCollectionResource = new {
                    value = shapedBusinessWithLinks, links = links
                };

                return Ok (linkedCollectionResource);

            } else {


                var previousPageLink = resultFromRepo.HasPrevious ? ResourceUriHelper.CreateResourceUri("GetBusiness",resourceParams,ResourceUriType.PreviousPage,_urlHelper) : null;
                var nextPageLink = resultFromRepo.HasNext ? ResourceUriHelper.CreateResourceUri ("GetBusiness",resourceParams, ResourceUriType.NextPage,_urlHelper) : null;
                
                var paginationMetadata = new {
                    previousPageLink = previousPageLink,
                    nextPageLink = nextPageLink,
                    totalCount = resultFromRepo.TotalCount,
                    pageSize = resultFromRepo.PageSize,
                    currentPage = resultFromRepo.CurrentPage,
                    totalPages = resultFromRepo.TotalPages
                };

                Response.Headers.Add ("X-Pagination",
                    Newtonsoft.Json.JsonConvert.SerializeObject (paginationMetadata));

                return Ok (businessList.ShapeData (resourceParams.Fields));

            }

        }
        #endregion


        
        // public IActionResult GetBusinessByAdsh(string adsh,[FromQuery] string fields)
        // {
        //     Contract.Ensures(Contract.Result<IActionResult>().IsNotNull());

        //     SubDto dto=null;

        //     if(!_typeHelperService.TypeHasProperties<SubDto>(fields))
        //         return BadRequest();

        //     if(!dataRepository.IsBusinessExists(adsh))
        //         return NotFound();

        //     TryCatchHelper.Execute(()=>{
        //         var resultFromRepo=dataRepository.GetBusinessByAdsh(adsh);
        //         dto=resultFromRepo.doMap<SubDto>();
        //     }).IfNotNull(ex=>{
        //        return StatusCode(500,"An Unhandled fault happend. Try again later"); 
        //     });

        //     if(dto.IsNotNull())
        //     {
        //         var links = ResourceUriHelper.CreateLinks("GetBusiness",adsh,fields,_urlHelper);
        //         var linkedResourceToReturn = dto.ShapeData(fields) as IDictionary<string, object>;

        //         linkedResourceToReturn.Add("links",links);

        //         return Ok(linkedResourceToReturn);
        //     }

        //     return NotFound();
        // }
        // public IActionResult Error () {
        //     ViewData["RequestId"] = Activity.Current?.Id ?? HttpContext.TraceIdentifier;
        //     return View ();
        // }

        
    }
}