namespace vd.web.Services {
    public interface ITypeHelperService {
        bool TypeHasProperties<T> (string fields);
    }
}