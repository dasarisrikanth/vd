using System.Collections.Generic;
using vd.database.Entity;
using vd.web.Entittes;
using vd.web.Helpers;

namespace vd.web.Services 
{
    public interface IDataRepository 
    {
        #region Submissions 
        PagedList<submission> GetBusiness (ResourceParams param);

        submission GetBusinessByAdsh (string _adsh);

        bool IsBusinessExists(string adsh);
        #endregion

        #region Presenation Data 
        PagedList<pre> GetPresentationData(ResourceParams param);

        bool IsPresentationDataExist(string adsh);
        #endregion

        #region Numbers 

        PagedList<num> GetNumbers(ResourceParams param);

        bool IsNumbersExist(string adsh);
        #endregion        

    }
}