using System.Collections.Generic;
using System.Linq;
using vd.core.extensions;
using vd.database;
using vd.database.Entity;
using vd.web.Entittes;
using vd.web.Extensions;
using vd.web.Helpers;

namespace vd.web.Services 
{
    public class DataRepository : IDataRepository 
    {

        private VDContext _context;
        private IPropertyMappingService _propertyMappingService;

        public DataRepository (VDContext context, IPropertyMappingService propertyMappingService) 
        {
            _context = context;
            _propertyMappingService = propertyMappingService;
        }

        public bool IsBusinessExists(string adsh)
        {
            return _context.submissions.Any (a => a.adsh.ToLowerInvariant () == adsh.Trim ().ToLowerInvariant ());
        }

        public PagedList<submission> GetBusiness (ResourceParams param) 
        {
            var collectionBeforePaging = _context.submissions.ApplySort (param.OrderBy, _propertyMappingService.GetPropertyMapping<SubDto, submission> ());

            if (param.Adsh.IsNotNull ()) {
                var adshForWhereClause = param.Adsh.Trim ().ToLowerInvariant ();

                collectionBeforePaging = collectionBeforePaging.Where (a => a.adsh.ToLowerInvariant ().Equals(adshForWhereClause));
            }

            if (param.SearchQuery.IsNotEmpty ()) {
                var searchQueryForWhereClause = param.SearchQuery.Trim ().ToLowerInvariant ();

                collectionBeforePaging = collectionBeforePaging.Where (a => a.adsh.ToLowerInvariant ().Equals (searchQueryForWhereClause) ||
                    a.name.ToLowerInvariant ().Contains (searchQueryForWhereClause));
            }

            return PagedList<submission>.Create (collectionBeforePaging, param.PageNumber, param.PageSize);
        }

        public submission GetBusinessByAdsh (string _adsh) 
        {
            return _context.submissions.Where (a => a.adsh.ToLowerInvariant () == _adsh.Trim ().ToLowerInvariant ()).FirstOrDefault();
        }


        public PagedList<pre> GetPresentationData(ResourceParams param)
        {
            var collectionBeforePaging=_context.pres.ApplySort(param.OrderBy, _propertyMappingService.GetPropertyMapping<PreDto,pre>());

            if(param.Adsh.IsNotNull())
            {
                var adshForWhereClause=param.Adsh.Trim().ToLowerInvariant();
                collectionBeforePaging =collectionBeforePaging.Where(a=>a.adsh.ToLowerInvariant().Equals(adshForWhereClause));
            }

            if (param.Statement.IsNotEmpty()) 
            {
                var stmtQueryForWhereClause = param.Statement.Trim().ToLowerInvariant();
                collectionBeforePaging = collectionBeforePaging.Where(a=>a.stmt.ToLowerInvariant().Equals(stmtQueryForWhereClause));
            }

            if (param.Tags.IsNotEmpty()) 
            {
                var tagsQueryForWhereClause = param.Tags.Trim().ToLowerInvariant();
                collectionBeforePaging = collectionBeforePaging.Where(a=>a.tag.ToLowerInvariant().Equals(tagsQueryForWhereClause));
            }

            return PagedList<pre>.Create(collectionBeforePaging,param.PageNumber,param.PageSize);
        }

        public bool IsPresentationDataExist(string adsh)
        {
            return _context.pres.Any (a => a.adsh.ToLowerInvariant () == adsh.Trim ().ToLowerInvariant ());
        }

        public PagedList<num> GetNumbers(ResourceParams param)
        {
            var collectionBeforePaging = _context.nums.ApplySort(param.OrderBy,_propertyMappingService.GetPropertyMapping<NumDto,num>());

            if(param.Adsh.IsNotNull())
            {
                var adshForWhereClause=param.Adsh.Trim().ToLowerInvariant();
                collectionBeforePaging=collectionBeforePaging.Where(a=>a.adsh.ToLowerInvariant().Equals(adshForWhereClause));
            }

            if (param.Tags.IsNotEmpty()) 
            {
                var tagsQueryForWhereClause = param.Tags.Trim().ToLowerInvariant();
                collectionBeforePaging = collectionBeforePaging.Where(a=>a.tag.ToLowerInvariant().Equals(tagsQueryForWhereClause));
            }

            return PagedList<num>.Create(collectionBeforePaging,param.PageNumber,param.PageSize);
        }

        public bool IsNumbersExist(string adsh)
        {
            return _context.nums.Any (a => a.adsh.ToLowerInvariant () == adsh.Trim ().ToLowerInvariant ());
        }
    }
}