using System;
using System.Collections.Generic;
using System.IO.Compression;
using System.Linq;
using System.Threading.Tasks;
using AspNetCoreRateLimit;
using AutoMapper;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Formatters;
using Microsoft.AspNetCore.Mvc.Infrastructure;
using Microsoft.AspNetCore.Mvc.Routing;
using Microsoft.AspNetCore.ResponseCompression;
using Microsoft.AspNetCore.SpaServices.Webpack;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json.Serialization;
using NLog.Extensions.Logging;
using Pomelo.EntityFrameworkCore.MySql;
using vd.core.extensions;
using vd.database;
using vd.database.Entity;
using vd.web.Entittes;
using vd.web.Extensions;
using vd.web.MappingProfiles;
using vd.web.Services;
using vd.solr;
namespace vd.web {
    public class Startup {
        public Startup (IConfiguration configuration) {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices (IServiceCollection services) {

            //this needs to be removed...
            services.AddCors();


            services.AddMvc (setupAction => {
                    setupAction.ReturnHttpNotAcceptable = true;

                    var jsonInputFormatter = setupAction.InputFormatters
                        .OfType<JsonInputFormatter> ().FirstOrDefault ();

                    if (jsonInputFormatter.IsNotNull ()) {
                        jsonInputFormatter.SupportedMediaTypes.Add("application/vd.webapi.hateoas+json");
                    }

                    var jsonOutputFormatter = setupAction.OutputFormatters
                        .OfType<JsonOutputFormatter> ().FirstOrDefault ();

                    if (jsonOutputFormatter.IsNotNull ()) {
                        jsonOutputFormatter.SupportedMediaTypes.Add("application/vd.webapi.hateoas+json");
                    }

                })
                .AddJsonOptions (options => {
                    options.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver ();
                });

            services.AddResponseCompression (options => {
                options.Providers.Add<GzipCompressionProvider> ();
            });

            services.Configure<GzipCompressionProviderOptions> (options => {
                options.Level = CompressionLevel.Fastest;
            });

            var connectionString = Configuration["connectionStrings:VDConnectionString"];
            services.AddDbContext<VDContext> (o => o.UseMySql (connectionString));

            // services.Configure<IpRateLimitOptions>((options) =>
            // {
            //     options.GeneralRules = new System.Collections.Generic.List<RateLimitRule>()
            //     {
            //         new RateLimitRule()
            //         {
            //             Endpoint = "*",
            //             Limit = 1000,
            //             Period = "5m"
            //         },
            //         new RateLimitRule()
            //         {
            //             Endpoint = "*",
            //             Limit = 200,
            //             Period = "10s"
            //         }
            //     };
            // });

            services.AddSingleton (typeof (IRepository<>), typeof (Repository<>))
                    .AddScoped<IDataRepository, DataRepository> ()
                    .AddScoped<IRepository<BaseEntity>, Repository<BaseEntity>> ()
                    .AddScoped<IRepository<num>, Repository<num>> ()
                    .AddScoped<IRepository<submission>, Repository<submission>> ()
                    .AddScoped<IRepository<pre>, Repository<pre>> ()
                    .AddScoped<IRepository<tag>, Repository<tag>> ()
                    .AddTransient<IPropertyMappingService, PropertyMappingService> ()
                    .AddTransient<ITypeHelperService, TypeHelperService> ()
                    .AddSingleton<IActionContextAccessor, ActionContextAccessor> ()
                    .AddSingleton<IVdSearch,VdSearch>()
                    .AddScoped<IUrlHelper> (implementationFactory => {
                        var actionContext = implementationFactory.GetService<IActionContextAccessor> ().ActionContext;
                        return new UrlHelper (actionContext);
                    });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure (IApplicationBuilder app, IHostingEnvironment env,ILoggerFactory loggerFactory) {

            loggerFactory.AddConsole();
            loggerFactory.AddDebug(LogLevel.Information);
            loggerFactory.AddNLog();

            if (env.IsDevelopment ()) {
                app.UseDeveloperExceptionPage ();
                app.UseWebpackDevMiddleware (new WebpackDevMiddlewareOptions {
                    HotModuleReplacement = true,
                        ReactHotModuleReplacement = true
                });
            } else {
                app.UseExceptionHandler ("/Home/Error");
            }

            app.UseResponseCompression ();
            app.UseStaticFiles ();
            //Custom Middleware  --START--
            app.UseResponseWrapper ();
            //Custom Middleware  --END--

            //this needs to be removed...
            app.UseCors(options=>options.WithOrigins("http://localhost:5000").AllowAnyMethod());

            app.UseMvc (routes => {
                routes.MapRoute (
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");

                routes.MapSpaFallbackRoute (
                    name: "spa-fallback",
                    defaults : new { controller = "Home", action = "Index" });
            });

            
            //app.UseIpRateLimiting();

            //app.UseHttpCacheHeaders();

            Mapper.Initialize (c =>
                c.AddProfiles (new [] {
                    typeof (NumMappingProfile),
                    typeof (SubMappingProfile),
                    typeof (TagMappingProfile),
                    typeof (PreMappingProfile),
                    typeof(SearchMappingProfile)
                }));

        }

        
    }
}