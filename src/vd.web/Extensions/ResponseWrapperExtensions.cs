using Microsoft.AspNetCore.Builder;
using vd.web.Middleware;

namespace vd.web.Extensions {
    public static class ResponseWrapperExtensions {
        public static IApplicationBuilder UseResponseWrapper (this IApplicationBuilder builder) {
            return builder.UseMiddleware<ResponseWrapper> ();
        }
    }
}