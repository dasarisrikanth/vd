using System;
using System.Collections.Generic;
using System.Reflection;
using AutoMapper;
using vd.database.Entity;
using vd.web.Helpers;

namespace vd.web.Extensions {
    public static class MapperExtension {
        public static T doMap<T> (this object source) {
            return AutoMapper.Mapper.Map<T> (source);
        }

        public static IEnumerable<TDest> PagedMap<TSource, TDest> (this TSource source) where TSource : PagedList<submission> {
            var list = new List<TDest> ();
            foreach (var x in source) {
                list.Add (x.doMap<TDest> ());
            }
            return list;
        }

        // public static TModel ToModel<TModel> (this PagedList<T> entity) {
        //     return (TModel) Mapper.Map (entity, entity.GetType (), typeof (TModel));
        // }

        public static TDest UpdateDestination<TDest, TSource> (this object dest, TSource source) {
            return (TDest) Mapper.Map (source, dest, typeof (TSource), typeof (TDest));
        }

        public static IMappingExpression<TSource, TDestination> IgnoreAllNonExisting<TSource, TDestination>
            (this IMappingExpression<TSource, TDestination> expression) {
                var flags = BindingFlags.Public | BindingFlags.Instance;
                var sourceType = typeof (TSource);
                var destinationProperties = typeof (TDestination).GetProperties (flags);

                foreach (var property in destinationProperties) {
                    if (sourceType.GetProperty (property.Name, flags) == null) {
                        expression.ForMember (property.Name, opt => opt.Ignore ());
                    }
                }
                return expression;
            }

    }
}