namespace vd.web.Constants
{
    public static class ConstMediaType
    {
        public const string VD_API_HATEOAS = "application/vd.webapi.hateoas+json";
    }
}