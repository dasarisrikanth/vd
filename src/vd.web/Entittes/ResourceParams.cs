namespace vd.web.Entittes {
    public class ResourceParams {
        public int maxPageSize = 20;

        public int PageNumber { get; set; } = 1;

        private int _pageSize = 10;
        public int PageSize {
            get { return _pageSize; }
            set {
                _pageSize = (value > maxPageSize) ? maxPageSize : value;
            }
        }

        public string Adsh { get; set; }

        public string SearchQuery { get; set; }

        public string Statement {get;set;}

        public string Tags {get;set;}

        public string OrderBy { get; set; } = "adsh";

        public string Fields { get; set; }
    }
}