namespace vd.web.Entittes {
    public class PreDto {

        public string adsh { get; set; }

        public decimal report { get; set; }

        public decimal line { get; set; }

        public string stmt { get; set; }

        public bool inpth { get; set; }

        public string rfile { get; set; }

        public string tag { get; set; }

        public string version { get; set; }

        public string plabel { get; set; }

        public string negating { get; set; }
    }
}