using System;
using System.Diagnostics;
using vd.core.Constants;
using vd.core.extensions;

namespace vd.core.Helper
{
    public static class TryCatchHelper
    {
        public static Exception Execute(Action action)
        {
            var ex = _do(action);
            if(ex.IsNotNull())
            {
                Debug.Write(ex.Message + "\n" + ex.InnerException + "\n" + ex.StackTrace);
            }
            return ex;
        }

        public static Exception Execute(Func<string> tag, Action action)
        {

            var ex = _do(action);

            if(ex.IsNotNull())
            {
                var ctx = tag.Call();
                if (ctx.IsNotEmpty())
                    ex.Data[ConstExceptions.Context] = ctx;

                Debug.Write(ex.Message + "\n" + ex.InnerException + "\n" + ex.StackTrace);
            }

            return ex;
        }

        private static Exception _do(Action action)
        {
            Exception noexp = null;
            try
            {
                action();
            }
            catch (Exception ex)
            {
                noexp = ex;
            }

            return noexp;
        }
    }
}