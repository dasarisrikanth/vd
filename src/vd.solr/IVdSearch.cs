using vd.solr.Models;

namespace vd.solr
{
    public interface IVdSearch
    {
         QueryResponse DoSearch(SolrQueryObj query);
         void Connect(string coreurl);

         bool IsConnected {get;set;} 
    }
}