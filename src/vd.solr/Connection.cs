
using System;
using CommonServiceLocator;
using SolrNet;
using vd.solr.Models;

namespace vd.solr
{
    internal class Connection
    {
        internal Connection(string coreUrl)
        {
            InitilizeConnection(coreUrl);
        }

        private void InitilizeConnection(string coreUrl)
        {
            Startup.Init<SolrResultObj>(coreUrl);
        }

        internal ISolrOperations<SolrResultObj> GetSolrInstance()
        {
            
            return ServiceLocator.Current.GetInstance<ISolrOperations<SolrResultObj>>();
            //return services.GetService<ISolrOperations<SolrResultObj>>();
        }
    }
}