using System.Collections.Generic;
using SolrNet.Commands.Parameters;

namespace vd.solr
{
    internal class Highlights
    {
        internal HighlightingParameters BuildHighlightParameters()
        {
            HighlightingParameters parameters = new HighlightingParameters()
            {
                Fields = new List<string>() { "name"},
                Fragsize = 200
            };

            return parameters;
        }
    }
}