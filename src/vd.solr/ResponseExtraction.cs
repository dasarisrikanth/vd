using System.Collections.Generic;
using System.Linq;
using SolrNet;
using SolrNet.Impl;
using vd.solr.Models;

namespace vd.solr
{
    internal class ResponseExtraction
    {
        //Extract parts of the SolrNet response and set them in QueryResponse class
        internal void SetHeader(QueryResponse queryResponse, SolrQueryResults<SolrResultObj> solrResults)
        {
            queryResponse.QueryTime = solrResults.Header.QTime;
            queryResponse.Status = solrResults.Header.Status;
            queryResponse.TotalHits = solrResults.NumFound;
        }

        internal void SetBody(QueryResponse queryResponse, SolrQueryResults<SolrResultObj> solrResults)
        {
            queryResponse.Results = (List<SolrResultObj>)solrResults;

            foreach (SolrResultObj obj in queryResponse.Results)
            {
                // if (solrResults.Highlights.ContainsKey(course.CourseId))
                // {
                //     HighlightedSnippets snippets = solrResults.Highlights[course.CourseId];

                //     if (snippets.ContainsKey("coursetitle"))
                //     {
                //         course.CourseTitle = snippets["coursetitle"].FirstOrDefault();
                //     }

                //     if (snippets.ContainsKey("description"))
                //     {
                //         course.CourseTitle = snippets["description"].FirstOrDefault();
                //     }
                // }
            }
        }

        internal void SetSpellcheck(QueryResponse queryResponse, SolrQueryResults<SolrResultObj> solrResults)
        {
            List<string> spellSuggestions = new List<string>();

            foreach (SpellCheckResult spellResult in solrResults.SpellChecking)
            {
                foreach (string suggestion in spellResult.Suggestions)
                {
                    spellSuggestions.Add(suggestion);
                }
            }

            queryResponse.DidYouMean = spellSuggestions;
        }
        
    }
}