using System.Collections.Generic;
using SolrNet;
using SolrNet.Commands.Parameters;
using vd.solr.Models;

namespace vd.solr
{
    internal class FiltersFacets
    {
        internal ICollection<ISolrQuery> BuildFilterQueries(SolrQueryObj query)
        {
            ICollection<ISolrQuery> filters=new List<ISolrQuery>();

            List<SolrQueryByField> refiners=new List<SolrQueryByField>();

            foreach (string item in query.Filter)
            {
                refiners.Add(new SolrQueryByField("name", item));
            }

            if(refiners.Count>0)
                filters.Add(new SolrMultipleCriteriaQuery(refiners, "OR"));

            return filters;
        }

        internal FacetParameters BuildFacets()
        {
            return new FacetParameters
            {
                Queries = new List<ISolrFacetQuery>{
                                new SolrFacetFieldQuery("name"){MinCount = 1}
                                
                }
            };
        }
    }
}