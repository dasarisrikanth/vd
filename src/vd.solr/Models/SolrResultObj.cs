using SolrNet.Attributes;

namespace vd.solr.Models
{
    public class SolrResultObj
    {
        [SolrUniqueKey("adsh")]
        public string adsh {get;set;}

        [SolrField("name")]
        public string name {get;set;}
    }
}