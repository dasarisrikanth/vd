using System.Collections.Generic;

namespace vd.solr.Models
{
    public class SolrQueryObj
    {
        public SolrQueryObj()
        {   
            Rows=10;
            Start=0;
            Filter=new List<string>();
        }

        public string Query {get;set;}

        public int Start {get;set;}

        public int Rows {get;set;}

        public List<string> Filter {get;set;}
    }
}