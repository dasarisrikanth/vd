using System.Collections.Generic;
using SolrNet;

namespace vd.solr.Models
{
    public class QueryResponse
    {
        
        public List<SolrResultObj> Results {get;set;}

        public int TotalHits {get;set;}

        public int QueryTime {get;set;}

        public int Status {get;set;}

        public SolrQueryObj Query {get;set;}

        public List<string> DidYouMean {get;set;}
        
    }
}