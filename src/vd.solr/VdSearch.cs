using System;
using CommonServiceLocator;
using Microsoft.Extensions.Logging;
using NLog.Extensions.Logging;
using SolrNet;
using SolrNet.Commands.Parameters;
using vd.solr.Models;
using vd.core;
using Microsoft.Extensions.DependencyInjection;
using vd.core.extensions;

namespace vd.solr
{
    public class VdSearch:IVdSearch
    {
        #region Private Memebers
        private Connection connection;
        private IServiceCollection serviceCollection;
        #endregion

        public VdSearch()
        {
            serviceCollection=new ServiceCollection(); 
            serviceCollection
                    .AddSingleton<ILoggerFactory, LoggerFactory>()
                    .AddLogging();
            
            var loggerFactory = serviceCollection.BuildServiceProvider().GetRequiredService<ILoggerFactory>();
            
            loggerFactory.AddNLog(new NLogProviderOptions { CaptureMessageTemplates = true, CaptureMessageProperties =true });
            loggerFactory.ConfigureNLog(StaticUtilities.GetDataPath()+"/nlog.config"); 
        }

        public bool IsConnected { get; set; }

        public void Connect(string coreurl)
        {
            connection=new Connection(coreurl);   
            if(connection.IsNotNull())
                IsConnected=true;
        }

        public QueryResponse DoSearch(SolrQueryObj query)
        {
            //FiltersFacets filtersFacets=new FiltersFacets();
            //Highlights highlights=new Highlights();
            //ExtraParameters extraParameters=new ExtraParameters();

            SolrQueryResults<SolrResultObj> solrResults;
            QueryResponse queryResponse=new QueryResponse();

            queryResponse.Query=query;

            ISolrOperations<SolrResultObj> solr=connection.GetSolrInstance();

            QueryOptions queryOptions = new QueryOptions
            {
                Rows = query.Rows,
                StartOrCursor = new StartOrCursor.Start(query.Start),
                //FilterQueries = filtersFacets.BuildFilterQueries(query),
                //Facet = filtersFacets.BuildFacets(),
                //Stats = filtersFacets.BuildStats(),
                //Highlight = highlights.BuildHighlightParameters(),
                //ExtraParams = extraParameters.BuildExtraParameters()
            };

            //Execute the query
            ISolrQuery solrQuery = new SolrQuery(query.Query);

            solrResults = solr.Query(solrQuery, queryOptions);

           //Set response
            ResponseExtraction extractResponse = new ResponseExtraction();

            extractResponse.SetHeader(queryResponse, solrResults);
            extractResponse.SetBody(queryResponse, solrResults);
            extractResponse.SetSpellcheck(queryResponse, solrResults);
            //extractResponse.SetFacets(queryResponse, solrResults);

            //Return response;
            return queryResponse;

        }
    }
}