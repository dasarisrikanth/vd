using Microsoft.VisualStudio.TestTools.UnitTesting;
using vd.solr.Models;

namespace vd.solr.tests
{
    [TestClass]
    public class VDSearchTests
    {
        [TestMethod]
        public void VDSearchTests_DoSearch_Tests_withData()
        {
            if(tsearch.IsConnected)
            {
                var response = tsearch.DoSearch(tquery);

                Assert.IsNotNull(response);         
            }   
        }

        [TestInitialize]
        public void Setup()
        {
            tsearch = new VdSearch();
            tsearch.Connect("http://localhost:8983/solr/vdash");
            tquery = new SolrQueryObj();
            tquery.Query="cognizant";
        }

        private VdSearch tsearch;
        private SolrQueryObj tquery;
         
    }
}
